package com.holiday.repository;

import com.holiday.model.Holidays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface HolidaysRepository extends JpaRepository<Holidays, Integer>{
     Iterable<Holidays> findByDate(int date);
     Iterable<Holidays> findByMonth(String month);
     Iterable<Holidays> findByWeekday(String weekday);
     Iterable<Holidays> findByName(String name);

}