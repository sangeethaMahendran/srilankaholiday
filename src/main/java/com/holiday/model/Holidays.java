package com.holiday.model;

import org.springframework.data.annotation.Immutable;

import javax.persistence.*;

@Entity
@Immutable
@Table(name="holidays")
public class Holidays {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public int hid;
    public int date;
    public String month;
    public String weekday;
    public String name;
    public String type;

    public int getHid(){
        return hid;
    }
    public void setHid(int hid){
        this.hid=hid;
    }
    public int getDate(){
        return date;
    }
    public void setDate(int date)
    {
        this.date=date;
    }
    public String getMonth(){
        return month;
    }
    public void setMonth(String month){
        this.month=month;
    }
    public String getWeekday(){
        return weekday;
    }
    public void setWeekday(String weekday){
        this.weekday=weekday;
    }
    public String getName(){
        return name;
    }
    public void setName(String name){
        this.name=name;
    }
    public String getType(){
        return type;
    }
    public void setType(String type){
        this.type=type;
    }
}
