package com.holiday.controller;
import com.holiday.model.Holidays;
import com.holiday.service.HolidaysService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
public class HolidayController {

    private HolidaysService holidaysService;
    @Autowired
    public void setReListingService(HolidaysService holidaysService) {
        this.holidaysService = holidaysService;
    }
   @GetMapping("/Holidays/date/{date}")
    public @ResponseBody Iterable<Holidays> getHolidaysByDATE(@PathVariable int date){
        return holidaysService.getAllHolidaysByDate(date);
    }
    @GetMapping("/Holidays/month/{month}")
    public @ResponseBody Iterable<Holidays> getHolidaysByMonth(@PathVariable String month){
        return holidaysService.getAllHolidaysByMonth(month);
    }
    @GetMapping("/Holidays")
    public @ResponseBody Iterable<Holidays> readAll(){
        return holidaysService.getAllHolidays();
    }

    @GetMapping("/Holidays/weekday/{weekday}")
    public @ResponseBody Iterable<Holidays> getHolidaysByWeekday(@PathVariable String weekday){
        return holidaysService.getAllHolidaysByWeekday(weekday);
    }
    @GetMapping("/Holidays/name/{name}")
    public @ResponseBody Iterable<Holidays> getHolidaysByName(@PathVariable String name){
        return holidaysService.getAllHolidaysByName(name);
    }
}
