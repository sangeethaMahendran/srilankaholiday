package com.holiday.service;

import com.holiday.model.Holidays;
import com.holiday.repository.HolidaysRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HolidaysService {
    private HolidaysRepository holidaysRepository;
    @Autowired
    public void setReListingRepository(HolidaysRepository holidaysRepository) {
        this.holidaysRepository = holidaysRepository;
    }

    public Iterable<Holidays> getAllHolidays()
    {
        return holidaysRepository.findAll();
    }
    public Iterable<Holidays> getAllHolidaysByDate(int DATE)
    {
        return holidaysRepository.findByDate(DATE);
    }
    public Iterable<Holidays> getAllHolidaysByMonth(String Month)
    {
        return holidaysRepository.findByMonth(Month);
    }
    public Iterable<Holidays> getAllHolidaysByWeekday(String weekday)
    {
        return holidaysRepository.findByWeekday(weekday);
    }
    public Iterable<Holidays> getAllHolidaysByName(String name)
    {
        return holidaysRepository.findByName(name);
    }
}
